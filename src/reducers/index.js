import { combineReducers } from 'redux';
import users from './usersReducer';
import app from './appReducer';

const rootReducers = combineReducers({users, app});

export default rootReducers;
