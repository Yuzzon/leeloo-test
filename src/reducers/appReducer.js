import { HIDE_LOADER, SHOW_LOADER, OPEN_MODAL, CLOSE_MODAL, SHOW_ORDERS_LOADER, HIDE_ORDERS_LOADER } from '../actions/types';

const inititalState = {
   loading: false,
   ordersLoading: false,
   openModal: false
}

export default (state = inititalState, action) => {
   switch (action.type) {
      case SHOW_LOADER:
         return {...state, loading:true}
      case HIDE_LOADER:
         return {...state, loading: false}
      case SHOW_ORDERS_LOADER:
         return {...state, ordersLoading:true}
      case HIDE_ORDERS_LOADER:
         return {...state, ordersLoading: false}
      case OPEN_MODAL:
         return {...state, openModal: true}
      case CLOSE_MODAL:
         return {...state, openModal: false}
      default:
         return state;
   }
}
