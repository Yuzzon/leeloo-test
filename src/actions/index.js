import { SHOW_LOADER, HIDE_LOADER, SET_PAGE, OPEN_MODAL, CLOSE_MODAL, SHOW_ORDERS_LOADER, HIDE_ORDERS_LOADER } from './types'

export const showLoader = () => {
   return {
      type: SHOW_LOADER
   }
}

export const hideLoader = () => {
   return {
      type: HIDE_LOADER
   }
}

export const showOrdersLoader = () => {
   return {
      type: SHOW_ORDERS_LOADER
   }
}

export const hideOrdersLoader = () => {
   return {
      type: HIDE_ORDERS_LOADER
   }
}

export const setPage = (pageNum) => {
   return {
      type: SET_PAGE,
      payload: pageNum
   }
}

export const openModal = () => {
   return {
      type: OPEN_MODAL
   }
}

export const closeModal = () => {
   return {
      type: CLOSE_MODAL
   }
}
