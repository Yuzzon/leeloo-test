export const convertDate = (dateBefore) => {
   var b = dateBefore.split(/\D+/);
   const date = new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]))
   let year = date.getFullYear();
   let month = date.getMonth() + 1;
   let dt = date.getDate();
   let time = date.toLocaleTimeString('en',
      { timeStyle: 'short', hour12: false, timeZone: 'UTC' });

   if (dt < 10) {
      dt = '0' + dt;
   }
   if (month < 10) {
      month = '0' + month;
   }
   return (dt + '-' + month + '-' + year + ' ' + time);
}
