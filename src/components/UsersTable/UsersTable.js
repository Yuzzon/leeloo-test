import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { fetchUsers, getUserOrders } from '../../actions/thunx';
import Pagination from '../Pagination/Pagination';
import { convertDate } from '../../helpers'
import { CircularProgress, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Button } from '@material-ui/core';
import './UsersTable.styles.scss'

const UsersTable = () => {
   const dispatch = useDispatch();
   const usersData = useSelector(state => state.users.usersList.data);
   const loading = useSelector(state => state.app.loading);
   const pageNumber = useSelector(state => state.users.pageNum);

   useEffect(() => {
      dispatch(fetchUsers());
   }, [dispatch])

   return (
      <TableContainer component={Paper}>
         <Table className="table">
            <TableHead className="table-head">
               <TableRow>
                  <TableCell align="center">#</TableCell>
                  <TableCell align="right">Ім'я</TableCell>
                  <TableCell align="right">Мессенджер</TableCell>
                  <TableCell align="right">Час останнього повідомлення</TableCell>
                  <TableCell align="right">Додаткова інформація</TableCell>
               </TableRow>
            </TableHead>
            <TableBody>
               {!loading && usersData ?
                  usersData.map((row, i) => (
                     <TableRow key={row.id}>
                        <TableCell align="center">
                           {(i + 1) + (pageNumber - 1) * 10}
                        </TableCell>
                        <TableCell align="right">{row.name}</TableCell>
                        <TableCell align="right">{row.from}</TableCell>
                        <TableCell align="right">{convertDate(row.lastMessageTime)}</TableCell>
                        <TableCell align="right">
                           <Button 
                           variant="contained"
                           className="orders-button"
                           onClick={() => dispatch(getUserOrders(row.id))}
                           >
                              Замовлення
                           </Button>
                        </TableCell>
                     </TableRow>
                  ))
                  :
                  <TableRow>
                     <TableCell style={{ border: "none", textAlign: "center" }}>
                        <CircularProgress />
                     </TableCell>
                  </TableRow>
               }
            </TableBody>
         </Table>
         <Pagination />
      </TableContainer>
   );
}
export default UsersTable
